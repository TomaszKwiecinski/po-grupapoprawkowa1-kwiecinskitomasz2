package com.company;

public class Main {

    public static void main(String[] args) {
        class Postac {
            public String imie;
            public int zywotnosc;
            public int punktyTaktyki;

            public void szal(int szal) {
                int nowa_zywotnosc = this.zywotnosc + szal;
                this.zywotnosc = Math.max(Math.min(100, nowa_zywotnosc), 0);

            }

            public int atak() {
                return punktyTaktyki * zywotnosc;
            }

            public String toString() {
                return "Postac " + imie + "(Zywotnosc= " + zywotnosc + "Punkty taktyki= " + punktyTaktyki;
            }
        }


        class Wojownik extends Postac {
            int sila, punktyTaktyki, zywotnosc;
            String imie;

            public Wojownik() {
                sila = 15;
                zywotnosc = 100;
                punktyTaktyki = 1;
                imie = "OrkA";
            }

            public int atak() {
                if (zywotnosc < 20)
                    return punktyTaktyki * zywotnosc * 150;
                return punktyTaktyki * zywotnosc * sila;
            }

            public String toString() {
                return "Wojownik " + imie + " zywotnosc=" + zywotnosc + "Punkty taktyki" + punktyTaktyki + "sila= " + sila;
            }

            class Lucznik extends Postac {
                int zrecznosc, punktyTaktyki, zywotnosc;
                String imie;

                public Lucznik() {
                    zrecznosc = 15;
                    zywotnosc = 100;
                    punktyTaktyki = 3;
                    imie = "GoblinA";
                }

                public int atak() {
                    return punktyTaktyki * zywotnosc * zrecznosc;
                }

                public String toString() {
                    return "Lucznik " + imie + "Zywotnosc= " + zywotnosc + "Punkty taktyki= " + punktyTaktyki + "zrecznosc= " + zrecznosc;
                }

                class DruzynaBohaterow {
                    {
                        Lucznik Legolas = new Lucznik();
                        Legolas.imie = "Legolas";
                        Wojownik Aragorn = new Wojownik();
                        Aragorn.imie = "Aragorn";

                        System.out.println(Legolas);
                        System.out.println(Aragorn);

                    }

                }
            }

        }

    }
}